__author__ = 'Mark Williams'
#

import apache_log_parser
import user_agents
import re
from csv import DictWriter

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s "%{User-agent}i"')

# variables used to search for personal identifying information
credit = re.compile(r'[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}')
email = re.compile(r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])")

# Create new file for anonymised data
with open('clean/clean_log.csv', 'w') as out_f:
    writer = DictWriter(out_f,
                        fieldnames=['remote_host',
                                    'time_received_isoformat',
                                    'request_method',
                                    'request_url_path',
                                    'request_url_query',
                                    'status',
                                    'request_header_user_agent'],
                        extrasaction='ignore')
    writer.writeheader()

    # client IP array
    ip_map ={}

    # open log
    with open('log/u_ex150601.log') as in_f:

        # loop over each line in log file
        for line in in_f:
            line = parser(line)
            ua = user_agents.parse(line['request_header_user_agent'])

            # Remove bots, spiders etc.
            if not ua.is_bot:
                ip_addr = line['remote_host']

                # Add IP to map if doesn't exist
                if ip_addr not in ip_map:
                    ip_map[ip_addr] = str(len(ip_map) + 1)

                # Replace IP with anonymous identifier
                line['remote_host'] = ip_map[ip_addr]

                # searches for personal details and anonymises it
                if 'request_url_query' in line:
                    if email.search(line['request_url_query']):
                        print('Anonymising email')
                        line['request_url_query'] = email.sub(line['request_url_query'], 'user@example.com')
                    if credit.search(line['request_url_query']):
                        print('Anonymising credit card')
                        line['request_url_query'] = credit.sub(line['request_url_query'], 'xxxx-xxxx-xxxx-xxxx')

                # Write each new line to file
                writer.writerow(line)
