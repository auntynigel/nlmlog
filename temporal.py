__author__ = 'Mark Williams'

import seaborn
from pandas import DataFrame
from matplotlib import pyplot
from datetime import datetime
from csv import DictReader

# variable arrays
data = {'day': [],
        'time': [],
        'category': []}

with open('clean/clean_log.csv') as in_f:
    # loop for each line in file
    for line in DictReader(in_f):
        # Search url for key words, add to array
        if '/events/displayevent' in line['request_url_path']:
            data['category'].append('events')
        elif '/maritime/' in line['request_url_path']:
            data['category'].append('Maritime')
        elif '/wml/' in line['request_url_path']:
            data['category'].append('WML')
        elif '/ism/exhibitions/' in line['request_url_path']:
            data['category'].append('ISM')
        elif '/onlineshop/' in line['request_url_path']:
            data['category'].append('Shop')
        elif '/mol/' in line['request_url_path']:
            data['category'].append('MOL')
        elif '/walker/exhibitions/' in line['request_url_path']:
            data['category'].append('Walker')
        # disposable category
        else:
            data['category'].append('misc')

            # get date values
        data['day'].append(datetime.strptime(line['time_received_isoformat'], '%Y-%m-%dT%H:%M:%S').day)
        data['time'].append(datetime.strptime(line['time_received_isoformat'], '%Y-%m-%dT%H:%M:%S').hour * 60 + datetime.strptime(line['time_received_isoformat'], '%Y-%m-%dT%H:%M:%S').minute)

data = DataFrame(data)

# create chart
plot = seaborn.violinplot(data=data, x='time', y='category')
plot.get_figure().savefig('category_plot/category_day.png')
pyplot.close()
